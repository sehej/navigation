import React from 'react';
import {StyleSheet, Text, View, Button, Image, ImageBackground} from 'react-native';
import { createDrawerNavigator, createStackNavigator, DrawerItems } from 'react-navigation';
import {Content, Container, Header, Body, Icon} from 'native-base';
import Splash from './components/Splash.js';
import LoginSignup from './components/LoginSignup.js'
import Dash from './components/Dash.js';
import Plans from './components/Plans.js'
import signup1 from './components/signup1.js';
import emailLogin from './components/emailLogin.js';
import bmi from './components/bmi.js';
import bmr from './components/bmr';
import bfp from './components/bfp';
import whr from './components/whr';
import plans from './components/Plans.js';
import SkipDash from './components/skipDash.js';
import diet from './components/diet';



export class App extends React.Component {
  render(){
    return(
      <Navi />

    );
  }
}

const customDrawer = (props)=>(

  <Container>
    <Header style={{ height:200, marginBottom:8, paddingRight:0, paddingLeft:0, alignContent:'center', alignItems:'center', backgroundColor:'white'}}>
      
      <ImageBackground source={require('./images/drawerBackground.jpg')} style={{height:'100%', width:'100%'}}>
      <Body style={{alignContent:'center', alignItems:'center'}}>
        <Image
          source={require('./images/download.jpeg')}
          style={{height:100, width:100, borderRadius:55, marginTop:'10%'}}
          />
          <Text style={{marginTop:10,color:'white'}}>Natalie Portman</Text>
      </Body>
      </ImageBackground>
    </Header>
    <Content>
      <DrawerItems {...props} />  
    </Content>
  </Container>

);
///srmite


 const Navi = createDrawerNavigator({

  Dashboard:{
    screen: Dash
  },

  Plans:{
    screen: Plans
  },

  'Track Weight': {
    screen: Plans
  },

  'BMI Calculator': {
    screen: bmi
  },

  Profile: {
    screen: Plans
  },

  'Refer and Earn': {
    screen: Plans
  },

  'Rate Us': {
    screen: Plans
  },

  'Share Us': {
    screen: Plans
  }
},
{
    initialRouteName: 'Dashboard',
    contentComponent: customDrawer,
    drawerOpenRoute : 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'drawerToggle',
  });

const RootStack = createStackNavigator({
  Initial:{
    screen:Splash
  },
  Welcome:{
    screen:LoginSignup
  },
  Dash:{
    screen:App
  },
  emailLogin:{
    screen:emailLogin
  },
  signup1:{
    screen:signup1
  },
  bmi:{
    screen:bmi
  },
  bmr:{
    screen:bmr
  },
  bfp:{
    screen:bfp
  },
  whr:{
    screen:whr
  },

});



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default RootStack;
